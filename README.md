# README #

## Ejercicio ##

Desarrollar un sistema con autenticación que permita a los clientes registrados calcular si un número es primo o no

#### Requisitos funcionales:####
- Debe haber dos roles de usuario correspondientes a: administradores y clientes
- Un cliente es creado luego de registrarse a través de un método de la API pidiéndoles nombre, email y contraseña obligatorios
- Un administrador puede ser creado exclusivamente por otro administrador, con email y contraseña obligatorios
- La creación del primer administrador queda a libre elección de implementación
- Un administrador puede bloquear a cualquier tipo de usuario para realizar cualquier acción, salvo a sí mismo
- Un cliente puede calcular si un número es primo o no, a través de un método de la API

#### Requisitos no funcionales:####
- Framework: LoopBack v2.x
- Base de datos: MySQL
- El sistema debe ser desarrollado como si fuera a ponerse en producción para un proyecto real, esperando una alta tasa de tráfico, con las medidas de seguridad correspondientes y la prolijidad de código para que pueda continuarlo otro desarrollador
- No hace falta documentación de ningún tipo
- Publicar el repositorio en algún servidor Git (Github / Bitbucket / GitLab)

### Instalación ###
    git clone git@bitbucket.org:mg3dem/sysgarage.git
    npm install

### Dev ###
    npm run-script dev

### Tests ###
    npm run-script test
    
### Production ###
    npm run-script prod
