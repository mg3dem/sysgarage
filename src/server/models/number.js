'use strict';

module.exports = function(Number) {

    /**
     * Given an input number, check if it is a prime number returning a boolean
     * @param {number} number Number to check if it is prime
     * @param {Function(Error, boolean)} callback
     */

    Number.isPrime = function(number, callback) {
        var k,
            limit = Math.sqrt(number);
        for (k = 2; k <= limit; k += 1) {
            if (number % k === 0) {
                callback(null, false);
                return;
            }
        }
        callback(null, true);
    };
};