'use strict';
var LoopBackContext = require('loopback-context');
module.exports = function(User) {
    /**
     * Set the user as admin
     * @param {number} id User ID
     * @param {Function(Error, boolean)} callback
     */

    User.setAsAdmin = function(userId, callback) {


        User.findOne({ where: { id: userId } }, function(err, usr) {
            if (err) { return callback(err); }
            User.addRole(usr, 'admin', callback);
        });
    };

    /**
     * Block a user based by id
     * @param {number} userId Id of user to block
     * @param {Function(Error)} callback
     */

    User.block = function(userId, callback) {
        if (LoopBackContext.getCurrentContext().get('currentUser') === userId) {
            var error = new Error('You can not block yourself!');
            error.status = 409;
            return callback(error);
        }
        User.findOne({ where: { id: userId } }, function(err, usr) {
            if (err) { return callback(err); }
            User.addRole(usr, 'blocked', callback);
        });
    };

    /**
     * Unblock a blocked user
     * @param {number} userId Id of user to unblock
     * @param {Function(Error)} callback
     */

    User.unblock = function(userId, callback) {
        if (LoopBackContext.getCurrentContext().get('currentUser') === userId) {
            var error = new Error('You can not block yourself!');
            error.status = 409;
            return callback(error);
        }
        var RoleMapping = User.app.models.RoleMapping;
        var Role = User.app.models.Role;
        User.findOne({ where: { id: userId } }, function(err, usr) {
            if (err) { return callback(err); }

            Role.findOne({ where: { name: 'blocked' } }, function(err, role) {
                if (err) { return callback(err); }
                RoleMapping.destroyAll({
                    principalId: usr.id,
                    roleId: role.id
                });
            });
        });
        callback(null, true);
    };

    User.addRole = function(usr, roleName, callback) {
        var RoleMapping = User.app.models.RoleMapping;
        var Role = User.app.models.Role;
        Role.findOne({ where: { name: roleName } }, function(err, role) {
            if (err) { return callback(err); }
            RoleMapping.create({
                principalType: RoleMapping.USER,
                principalId: usr.id,
                roleId: role.id
            }, function(err) {
                if (err) { return callback(err); }
                callback(null, true);
            });

        });
    };
};