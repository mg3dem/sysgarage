module.exports = function(app) {

    //Disable unused endpoints
    app.models.user.disableRemoteMethodByName('upsert');
    app.models.user.disableRemoteMethodByName('updateAll');
    app.models.user.disableRemoteMethodByName('prototype.updateAttributes');
    app.models.user.disableRemoteMethodByName('find');
    app.models.user.disableRemoteMethodByName('findById');
    app.models.user.disableRemoteMethodByName('findOne');
    app.models.user.disableRemoteMethodByName('deleteById');
    app.models.user.disableRemoteMethodByName('confirm');
    app.models.user.disableRemoteMethodByName('count');
    app.models.user.disableRemoteMethodByName('exists');
    app.models.user.disableRemoteMethodByName('resetPassword');
    app.models.user.disableRemoteMethodByName('upsertWithWhere');
    app.models.user.disableRemoteMethodByName('replaceOrCreate');
    app.models.user.disableRemoteMethodByName('prototype.__count__accessTokens');
    app.models.user.disableRemoteMethodByName('prototype.__create__accessTokens');
    app.models.user.disableRemoteMethodByName('prototype.__delete__accessTokens');
    app.models.user.disableRemoteMethodByName('prototype.__destroyById__accessTokens');
    app.models.user.disableRemoteMethodByName('prototype.__findById__accessTokens');
    app.models.user.disableRemoteMethodByName('prototype.__get__accessTokens');
    app.models.user.disableRemoteMethodByName('prototype.__updateById__accessTokens');
    app.models.user.disableRemoteMethodByName('createChangeStream');
    app.models.user.disableRemoteMethodByName('replaceById');

    var User = app.models.user;
    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;

    User.findOne({ where: { name: 'Admin', email: 'admin@sysgarage.com' } }, function(err, usr) {
        //just create an admin user for the first time
        if (null === usr) {
            User.create([
                { name: 'Admin', email: 'admin@sysgarage.com', password: 'sysgarage' }
            ], function(err, users) {

                //create the admin role
                Role.create({
                    name: 'admin'
                }, function(err, role) {
                    if (err) throw err;

                    //make the admin default user
                    role.principals.create({
                        principalType: RoleMapping.USER,
                        principalId: users[0].id
                    }, function(err) {
                        if (err) throw err;
                    });
                });
                Role.create({
                    name: 'blocked'
                }, function(err) {
                    if (err) throw err;
                });
            });
        }

        if (err) {
            throw err;
        }
    });

};